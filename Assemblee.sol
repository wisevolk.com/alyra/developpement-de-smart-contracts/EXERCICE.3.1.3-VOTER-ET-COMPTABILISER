pragma solidity >=0.4.22 <0.6.0;

contract Assemblee {

    address[] private membres;
    Decision[] private decisionsToVote;

    struct Decision {
        string description;
        uint votesPour;
        uint votesContre;
    }

    function rejoindre() public {
        membres.push(msg.sender);
    }

    function estMembre(address _utilisateur) public view returns (bool) {
        for(uint i=0; i<membres.length; i++){
            if(membres[i] == _utilisateur){
                return true;
            }
        }
        return false;
    }

    function proposerDecision(string memory _description) public {
        if(estMembre(msg.sender)){
            Decision memory decision = Decision({description:_description,votesPour:0,votesContre:0});
            decisionsToVote.push(decision);

        }
    }

    function voter(uint _indice, bool _vote) public {
        require(estMembre(msg.sender));
        Decision storage decision = decisionsToVote[_indice];
        if(_vote){
            decision.votesPour++;
        } else {
            decision.votesContre++;
        }
    }

    function comptabiliser(uint _indice) public view returns (int256) {
        Decision memory decision = decisionsToVote[_indice];

        return int256(decision.votesPour - decision.votesContre);
    }
}
